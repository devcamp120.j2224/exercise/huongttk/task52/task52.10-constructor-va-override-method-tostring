package com.devcamp.j53_basicjava.s10;

public class Person {
    public String name;
    public int age;
    public double weight;
    public long salary;
    public String[] pets;
   
    //  contructor(Hàm khởi tạo không có tham số) không có tham số
    public Person() {
        this.name = "kimhuong";
        this.age = 29;
        this.weight = 48.5;
        this.salary = 15000000;
        this.pets = new String[] {"dog", "cat", "habit"};
    }
    //  Hàm khởi tạo có 3 tham số (số tham số phụ thuộc vào mình cần)

    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }
     //  Hàm khởi tạo có 4 tham số (số tham số phụ thuộc vào mình cần)
    public Person(String name, int age, double weight, long salary) {
        /* C1:
         *  this.name = name;
            this.age = age;
            this.weight = weight;
            this.salary = salary;
        */
        // C2: dùng hàm có sắn 3 tham số ở phía trên
        this(name, age, weight);
        this.salary = salary;
       
    }
     //  Hàm khởi tạo có 4 tham số (số tham số phụ thuộc vào mình cần)
    public Person(String name, int age, double weight, String[] pets) {
        this(name, age, weight);
        this.pets = pets;
    }
    //  Hàm khởi tạo có 5 tham số (số tham số phụ thuộc vào mình cần)
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age, weight, salary);
        this.pets = pets;
    }
    
    // Phương thức hiển thị dữ liệu
    public void showInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Weight: " + this.weight);
        System.out.println("Salary: " + this.salary);
        System.out.println("Pets: " + this.pets);
    }

    // Override: phương thức giống hết lớp cha(giống cả tên và tham số)
    public String toString() {
        String strPerson = "{";
        strPerson += "Name: " + this.name + ", ";
        strPerson += "Age: " + this.age + ", ";
        strPerson += "Weight: " + this.weight + ", ";
        strPerson += "Salary: " + this.salary + ", ";
        strPerson += "Pets:[";
        if(this.pets != null){
            for(String pet : pets){
                strPerson += pet + ", ";
            }
        }
        strPerson += "]}";
        return strPerson;
    }
}
