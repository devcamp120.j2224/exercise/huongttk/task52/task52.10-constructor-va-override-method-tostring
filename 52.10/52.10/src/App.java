import java.util.ArrayList;

import com.devcamp.j53_basicjava.s10.Person;

public class App {
    public static void main(String[] args) throws Exception {
       // System.out.println("Hello, World!");
        Person person1 = new Person();
        ArrayList <Person> listPerson = new ArrayList<Person>();
       // System.out.println(person1);
        //person1.showInfo();

        Person person2 = new Person("Minh Duy", 29, 60.5);
        //person2.showInfo();

        Person person3 = new Person("Vina", 13, 40.3, 2000000);
        //person3.showInfo();

        Person person4 = new Person("Phomai", 17, 70.0, new String[] {"dog", "cat", "habit"});
        //person4.showInfo();

        Person person5 = new Person("Kim Huong", 29, 49.0,  250000000, new String[] {"dog", "cat", "habit"});
        //person5.showInfo();

        listPerson.add(person1);
        listPerson.add(person2);
        listPerson.add(person3);
        listPerson.add(person4);
        listPerson.add(person5);
        for(Person person : listPerson){
            //c1
            //person.showInfo();
            //C2
            System.out.println(person);

        }
    }
}
